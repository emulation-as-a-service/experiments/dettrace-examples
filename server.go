/*
go build server.go
bin/dettrace --epoch 2020-01-01,10:00:00 --clock-step 1000000 --network ./server &
curl localhost:8080
killall -9 dettrace
*/

package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

func main() {
	seconds := 0
	go func() {
		for {
			time.Sleep(1 * time.Second)
			seconds += 1
		}
	}()

	i := 0
	type request struct {
		string
		int
	}
	requests := []request{}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		time := time.Now().Format("15:04:05")
		out := fmt.Sprintf("request %v at %v (running for %v seconds), previous requests: %v\n", i, time, seconds, requests)
		io.WriteString(w, out)
		log.Print(out)
		i += 1
		requests = append(requests, request{time, seconds})
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
